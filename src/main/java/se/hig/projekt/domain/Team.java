package se.hig.projekt.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.UniqueConstraint;

/**
 * Domänet som hanterar ett lag.
 * 
 * @author Agila Akrobater
 * @version 2021-04-26
 */
@Entity
public class Team {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    private String name;
   
    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "team_seasons",
            joinColumns = {@JoinColumn(name = "team_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "season_id", nullable = false)},
            uniqueConstraints = {@UniqueConstraint(
                    columnNames = {"season_id", "team_id"},
                    name = "PK_team_seasons")})
    private List<Season> seasons;
    
    
    @ManyToOne
    private Sport sport;
    
    @JsonIgnore
    @OneToMany(mappedBy = "homeTeam")
    private List<Game> homeGames;

    @JsonIgnore
    @OneToMany(mappedBy = "visitorTeam")
    private List<Game> awayGames;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }

    public Sport getSport() {
        return sport;
    }
    
    public List<Game> getHomeGames() {
        return homeGames;
    }

    public List<Game> getAwayGames() {
        return awayGames;
    }
    
    @JsonIgnore
    public List<Game> getAllGames() {
        List<Game> allGames = new ArrayList();  
        allGames.addAll(homeGames);
        allGames.addAll(awayGames);
        return allGames;
    }
}
