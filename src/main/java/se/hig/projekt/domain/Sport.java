package se.hig.projekt.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * Domänet som hanterar en sport.
 * 
 * @author Agila Akrobater
 * @version 2021-04-09
 */

@Entity
@Table(name = "sport")
public class Sport implements Serializable{
    
    @Column(name = "name")
    private String name;
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    
    @JsonIgnore
    @OneToMany(mappedBy = "sport")
    private List<League> leagues;

    public Sport(){
        
    }
    public Sport(String name) {
        this.name = name;
    }
   
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public List<League> getLeagues() {
        return leagues;
    }
    
}

