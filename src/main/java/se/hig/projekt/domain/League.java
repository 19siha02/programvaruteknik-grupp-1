package se.hig.projekt.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Domänet som hanterar en liga.
 * 
 * @author Agila Akrobater
 * @version 2021-04-19
 */
@Entity
@Table(name = "league")
public class League {
   
    /**
     * The name of the league.
     */
    @Column(nullable = false, length = 45)
    private String name;
    /**
     * Points awarded to the winning team.
     */
    private int winValue;
    /**
     * Points awarded to the team in a draw.
     */
    private int tieValue;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    /**
     * Points awarded to the teams in overtime.
     */
    private int overtimeWinValue;
    private int overtimeLossValue;
    
    @ManyToOne
    @NotNull
    private Sport sport;
    
    @JsonIgnore
    @OneToMany(mappedBy = "league")
    private List<Season> seasons;


    public League(){
    }
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public int getWinValue() {
        return winValue;
    }

    public void setWinValue(int winValue) {
        this.winValue = winValue;
    }

    public int getTieValue() {
        return tieValue;
    }

    public void setTieValue(int tieValue) {
        this.tieValue = tieValue;
    }

    public int getOvertimeWinValue() {
        return overtimeWinValue;
    }

    public void setOvertimeWinValue(int overtimeWinValue) {
        this.overtimeWinValue = overtimeWinValue;
    }
    
    public int getOvertimeLossValue() {
        return overtimeLossValue;
    }
    
    public void setOvertimeLossValue(int overtimeLossValue) {
        this.overtimeLossValue = overtimeLossValue;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    
    public Sport getSport() {
        return this.sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }
    
    public List<Season> getSeasons() {
        return seasons;
    }
    
}

