package se.hig.projekt.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Domänet som hanterar en säsong.
 * 
 * @author Agila Akrobater
 * @version 2021-04-26
 */
@Entity
public class Season {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    private int maxOfRounds;
    private String name;

    @JsonIgnore
    @ManyToMany(mappedBy = "seasons")
    private List<Team> teams;

    @ManyToOne
    private League league;

    @JsonIgnore
    @OneToMany(mappedBy = "season")
    private List<Round> rounds;

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public League getLeague() {
        return league;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxOfRounds() {
        return maxOfRounds;
    }

    public void setMaxOfRounds(int maxOfRounds) {
        this.maxOfRounds = maxOfRounds;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

}
