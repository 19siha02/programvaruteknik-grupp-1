package se.hig.projekt.domain;

import se.hig.projekt.utilities.GameMetaData;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Date;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * Domän objektet för en match.
 * 
 * @author Agila Akrobater
 * @version 2021-04-26
 */
@Entity
public class Game implements Comparable<Game>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "home_team_fk")
    private Team homeTeam;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "visitor_team_fk")
    private Team visitorTeam;

    private String arena;
    
    
    private int attendance;

    @NotNull
    private Date gameDate;
    
    private int visitorScore;
    private int homeScore;
    
    @NotNull
    private EndState endState;

    
    @ManyToOne
    private Round round;

    @Override
    public int compareTo(Game o) {
        if(gameDate.before(o.getGameDate()))
            return -1;
        if(gameDate.after(o.getGameDate()))
            return 1;
        return 0;
    }

    public enum EndState {
        UNFINISHED("Inte spelad ännu"),
        REGULAR("Efter ordinarie tid"),
        OVERTIME("Efter övertid"),
        SHOOTOUT("Efter straffläggning");

        private final String fieldDescription;

        private EndState(String value) {
            fieldDescription = value;
        }

        @Override
        public String toString() {
            return fieldDescription;
        }
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getVisitorTeam() {
        return visitorTeam;
    }

    public void setVisitorTeam(Team visitorTeam) {
        this.visitorTeam = visitorTeam;
    }

    public String getArena() {
        return arena;
    }

    public void setArena(String arena) {
        this.arena = arena;
    }

    public int getAttendance() {
        return attendance;
    }

    public void setAttendance(int attendance) {
        this.attendance = attendance;
    }

    public Date getGameDate() {
        return gameDate;
    }

    public void setGameDate(Date gameDate) {
        this.gameDate = gameDate;
    }

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }

    public int getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(int homeScore) {
        this.homeScore = homeScore;
    }

    public int getVisitorScore() {
        return visitorScore;
    }

    public void setVisitorScore(int visitorScore) {
        this.visitorScore = visitorScore;
    }

    public EndState getEndState() {
        return endState;
    }

    public void setEndState(EndState endState) {
        this.endState = endState;
    }

    public void setGameMetaData(Map<String, GameMetaData> metaDataMap) {
        GameMetaData metaData = metaDataMap.get("metaData");
        if (metaData.getArena() != null || !metaData.getArena().isEmpty()) {
            this.setArena(metaData.getArena());
        }
        if (metaData.getAttendance() >= 0) {
            this.setAttendance(metaData.getAttendance());
        }
    }
    
    @JsonIgnore
    public Team getWinner(){
        if(endState != EndState.UNFINISHED){
            if(this.homeScore > this.visitorScore)
                return this.homeTeam;
            if(this.homeScore < this.visitorScore)
                return this.visitorTeam;
        }
        return null;
    }

    
}
