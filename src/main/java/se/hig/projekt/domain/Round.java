package se.hig.projekt.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Domänet som hanterar en runda.
 * 
 * @author Agila Akrobater
 * @version 2021-04-26
 */
@Entity
public class Round {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    
    private int roundNo;

    
    @ManyToOne
    private Season season;
    
    @JsonIgnore
    @OneToMany(mappedBy = "round")
    private List<Game> games;

    public Round(){}
    
    public Round(int roundNo, Season season) {
        this.roundNo = roundNo;
        this.season = season;
    }


    public int getId() {
        return id;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public int getRoundNo() {
        return roundNo;
    }

    public void setRoundNo(int roundNo) {
        this.roundNo = roundNo;
    }
    
    public List<Game> getGames(){
        return games;
    }
}
