package se.hig.projekt.utilities;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import se.hig.projekt.domain.Game;
import se.hig.projekt.domain.League;
import se.hig.projekt.domain.Season;
import se.hig.projekt.domain.Team;

/**
 * Tabellbyggaren.
 * En tabell består av flera {@link TableRow}.
 * 
 * @author Agila Akrobater
 * @version 2021-05-10
 */
public class Table extends ArrayList implements Serializable {

    
    
    private List<Season> seasonList;
    private Date startDate, endDate;
    private Integer startRound, endRound;
    private Boolean awayGames, homeGames;

    public Table(FilterMetaData metaData) {
        startDate = metaData.getStartDate();
        endDate = metaData.getEndDate();
        startRound = metaData.getStartRound();
        endRound = metaData.getEndRound();
        awayGames = metaData.getAwayGames();
        homeGames = metaData.getHomeGames();
    }

    public void buildTable() {

        Set<Team> teams = new HashSet<>();
        seasonList.stream().forEach(season -> teams.addAll(season.getTeams()));
        teams.stream().forEach(team -> {
            Stream<Game> gameStream = streamOfFilteredGames(team);

            TableRow tableRow = new TableRow();
            tableRow.teamName = team.getName();
            gameStream.forEach(game -> aggregateRow(tableRow, team, game));

            this.add(tableRow);
        });
        
        Collections.sort(this);
        Collections.reverse(this);

    }

    private void aggregateRow(TableRow tableRow, Team team, Game game) {
        tableRow.gamesPlayed++;
        
        League league = game.getRound().getSeason().getLeague();
        if (team.equals(game.getHomeTeam())) {
            tableRow.scored += game.getHomeScore();
            tableRow.scoredAgainst += game.getVisitorScore();
        } else {
            tableRow.scored += game.getVisitorScore();
            tableRow.scoredAgainst += game.getHomeScore();
        }

        if (team.equals(game.getWinner())) {
            switch (game.getEndState()) {
                case REGULAR:
                    tableRow.wins++;
                    tableRow.points += league.getWinValue();
                    break;
                case OVERTIME:
                    tableRow.overTimeWins++;
                    tableRow.points += league.getOvertimeWinValue();
                    break;
                case SHOOTOUT:
                    tableRow.overTimeWins++;
                    tableRow.points += league.getOvertimeWinValue();
                    break;
            }

        }
        else if (game.getHomeScore() == game.getVisitorScore()) {
            tableRow.draws++;
            tableRow.points += league.getTieValue();
        }

        else if (!team.equals(game.getWinner()) && game.getWinner() != null) {
            switch (game.getEndState()) {
                case REGULAR:
                    tableRow.losses++;
                    break;
                case OVERTIME:
                    tableRow.overTimeLosses++;
                    tableRow.points += league.getOvertimeLossValue();
                    break;
                case SHOOTOUT:
                    tableRow.overTimeLosses++;
                    tableRow.points += league.getOvertimeLossValue();
                    break;
            }
        }
    }

    private Stream<Game> streamOfFilteredGames(Team team) {

        List<Game> games = team.getAllGames();
        Stream<Game> gameStream = Filter.gameFilter(games, team, awayGames, homeGames);

      
        gameStream = Filter.dateFilter(gameStream, startDate, endDate);
        
        
        gameStream = Filter.roundFilter(gameStream, startRound, endRound);
        gameStream = Filter.seasonFilter(gameStream, seasonList);
        gameStream = gameStream.filter(game -> (game.getEndState() != Game.EndState.UNFINISHED));
        return gameStream;
    }

    public void setSeasonList(List<Season> seasonList) {
        this.seasonList = seasonList;
    }

    /**
     * En rad i tabellen.
     */
    private class TableRow implements Comparable<TableRow> {

        public String teamName;
        public int gamesPlayed;
        public int points;
        public int wins;
        public int overTimeWins;
        public int draws;
        public int losses;
        public int overTimeLosses;
        public int scored;
        public int scoredAgainst;

        @Override
        public int compareTo(TableRow o) {

            if (points > o.points) {
                return 1;
            }
            if (points < o.points) {
                return -1;
            }
            if (scored - scoredAgainst > o.scored - o.scoredAgainst) {
                return 1;
            }
            if (scored - scoredAgainst < o.scored - o.scoredAgainst) {
                return -1;
            }
            if (scored > o.scored) {
                return 1;
            }
            if (scored < o.scoredAgainst) {
                return -1;
            }

            return 0;
        }

    }

}
