package se.hig.projekt.utilities;

import java.sql.Date;
import java.util.List;
import java.util.stream.Stream;
import se.hig.projekt.domain.Game;
import se.hig.projekt.domain.Season;
import se.hig.projekt.domain.Team;

/**
 * Filter som filtrerar matcher i olika situationer.
 * 
 * @author Agila Akrobater
 * @version 2021-05-10
 */
public class Filter {

    public static Stream<Game> seasonFilter(Stream<Game> inStream, List<Season> seasonList) {

        return inStream.filter(game -> seasonList.contains(game.getRound().getSeason()));

    }

    public static Stream<Game> dateFilter(Stream<Game> inStream, Date startDate, Date endDate) {

        return inStream.filter(game -> game.getGameDate().getTime() >= startDate.getTime() && game.getGameDate().getTime() <= endDate.getTime());

    }

    public static Stream<Game> roundFilter(Stream<Game> inStream, int startRound, int endRound) {

        return inStream.filter(game -> game.getRound().getRoundNo() >= startRound && game.getRound().getRoundNo() <= endRound);

    }

    public static Stream<Game> gameFilter(List<Game> gameList, Team team, Boolean awayGames, Boolean homeGames) {
        Stream<Game> visitorGamesStream = Stream.empty();
        Stream<Game> homeGamesStream = Stream.empty();
        
        if (awayGames) {
            visitorGamesStream = gameList.stream().filter(game -> game.getVisitorTeam().getId() == team.getId());
        }
        if (homeGames) {
           homeGamesStream = gameList.stream().filter(game -> game.getHomeTeam().getId() == team.getId());
        }
        
        
        //gameStream = Stream.concat(visitorGamesStream, gameStream);
       // gameStream = Stream.concat(homeGamesStream, gameStream);
        
        return Stream.concat(visitorGamesStream, homeGamesStream);
    }
}
