package se.hig.projekt.utilities;

/**
 *  Metadata för en match för att smidigt kunna skicka allt via @RequestBody
 * 
 * @author Agila Akrobater
 * @version 2021-04-27
 */
public class GameMetaData {
    private int attendance;
    private String arena;

    public int getAttendance() {
        return attendance;
    }

    public void setAttendance(int attendance) {
        this.attendance = attendance;
    }

    public String getArena() {
        return arena;
    }

    public void setArena(String arena) {
        this.arena = arena;
    }
    
    

}
