package se.hig.projekt.utilities;

/**
 * @author Agila Akrobater
 * @version 2021-05-10
 */
public enum GameResult {
    
    WIN, LOSS, TIE
    
}
