package se.hig.projekt.utilities;

import java.sql.Date;
import java.util.List;

/**
 * FilterMetaData som vi har hand om som en primitiv datatyp. 
 * 
 * @author Agila Akrobater
 * @version 2021-05-10
 */
public class FilterMetaData {

    
    private List<Integer> seasonListId;
    private Date startDate, endDate;
    private Integer startRound, endRound;
    private Boolean awayGames = true, homeGames = true;

    public List<Integer> getSeasonListId() {
        return seasonListId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Integer getStartRound() {
        return startRound;
    }

    public Integer getEndRound() {
        return endRound;
    }

    public Boolean getAwayGames() {
        return awayGames;
    }

    public Boolean getHomeGames() {
        return homeGames;
    }
    
    public void setSeasonListId(List<Integer> seasonListId) {
        this.seasonListId = seasonListId;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setStartRound(Integer startRound) {
        this.startRound = startRound;
    }

    public void setEndRound(Integer endRound) {
        this.endRound = endRound;
    }

    public void setAwayGames(Boolean awayGames) {
        this.awayGames = awayGames;
    }

    public void setHomeGames(Boolean homeGames) {
        this.homeGames = homeGames;
    }
    
    
}
