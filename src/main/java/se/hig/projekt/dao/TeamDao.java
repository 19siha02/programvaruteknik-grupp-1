package se.hig.projekt.dao;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.hig.projekt.domain.Season;
import se.hig.projekt.domain.Team;

/**
 * @author Agila Akrobater
 * @version 2021-04-26
 */
@Repository
public interface TeamDao extends JpaRepository<Team, Integer> {
    
    default Team findByIdOrThrow(int id) {
        return findById(id).orElseThrow(() -> new RuntimeException("Laget existerar inte i databasen."));
    }

   
    default List<Map> updateSeasonList(Season season, Team team) {
        List<Season> seasonList = team.getSeasons();
        seasonList.add(season);
        team.setSeasons(seasonList);
        save(team);
        return team.getSeasons().stream()
                .map(s -> Map.of("id", s.getId(), "season", s.getName(), "league", s.getLeague().getName()))
                .collect(Collectors.toList());
    }

}
