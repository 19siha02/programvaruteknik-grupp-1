package se.hig.projekt.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.hig.projekt.domain.Season;

/**
 * @author Agila Akrobater
 * @version 2021-04-26
 */
@Repository
public interface SeasonDao extends JpaRepository<Season, Integer>{
    
    default Season findByIdOrThrow(Integer id) {
        return findById(id).orElseThrow(() -> new RuntimeException("Säsongen hittades inte"));
    }
    
}
