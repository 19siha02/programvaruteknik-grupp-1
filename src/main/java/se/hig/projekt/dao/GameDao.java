package se.hig.projekt.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.hig.projekt.domain.Game;

/**
 * @author Agila Akrobater
 * @version 2021-04-26
 */
@Repository
public interface GameDao extends JpaRepository<Game, Integer> {
    
    default Game findByIdOrThrow(Integer id) {
        return findById(id).orElseThrow(() -> new RuntimeException());
    }
    
}
