package se.hig.projekt.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.hig.projekt.domain.Round;

/**
 * @author Agila Akrobater
 * @version 2021-04-26
 */
@Repository
public interface RoundDao extends JpaRepository<Round, Integer> {

    default Round findByIdOrThrow(Integer id) {
        return findById(id).orElseThrow(() -> new RuntimeException("Omgången hittades inte"));
    }
    
}
