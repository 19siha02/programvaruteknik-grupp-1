package se.hig.projekt.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.hig.projekt.domain.Sport;

/**
 * @author Agila Akrobater
 * @version 2021-04-26
 */
@Repository
public interface SportDao extends JpaRepository<Sport, Integer> {
    
    default Sport findByIdOrThrow(int id) {
        return findById(id).orElseThrow(() -> new RuntimeException());
    }
    
}

