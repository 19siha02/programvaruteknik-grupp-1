package se.hig.projekt.resource;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.hig.projekt.business.add.AddNewSportService;
import se.hig.projekt.business.list.ListAllSportsService;
import se.hig.projekt.domain.Sport;

/**
 * Resource som hanterar sport operationer.
 * 
 * @author Agila Akrobater
 * @version 2021-04-16
 */
@RestController
@RequestMapping(value = "sport")
public class SportResource {

    @Autowired
    private AddNewSportService addService;

    @Autowired
    private ListAllSportsService listAllSportService;

    @PostMapping(value = "create")
    public Sport createSport(@RequestBody Sport sport) {
        addService.init(Map.of("sport", sport));
        return addService.execute();
    }

    @GetMapping(value = "listAll")
    public List<Map> getAllSports() {
        return listAllSportService.execute();
    }

}
