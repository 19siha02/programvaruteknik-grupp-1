package se.hig.projekt.resource;

import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.hig.projekt.business.add.AddTeamBySportIdService;
import se.hig.projekt.business.add.AddTeamToSeasonService;
import se.hig.projekt.business.list.ListGreatestLossByTeamService;
import se.hig.projekt.business.list.ListGreatestRunService;
import se.hig.projekt.business.list.ListGreatestWinByTeamService;
import se.hig.projekt.domain.Game;
import se.hig.projekt.utilities.FilterMetaData;
import se.hig.projekt.domain.Team;
import se.hig.projekt.utilities.GameResult;

/**
 * Resource som hanterar lag operationer.
 * 
 * @author Agila Akrobater
 * @version 2021-04-27
 */
@RestController
@RequestMapping(value = "team")
public class TeamResource {

    private final Date MIN_DATE = Date.valueOf("0000-01-01");
    private final Date MAX_DATE = Date.valueOf("4628-01-01");

    @Autowired
    private AddTeamToSeasonService addTeamToSeasonService;

    @Autowired
    private AddTeamBySportIdService addTeamBySportService;

    @Autowired
    private ListGreatestWinByTeamService listGreatestWinByTeamService;

    @Autowired
    private ListGreatestLossByTeamService listGreatestLossByTeamService;

    @Autowired
    private ListGreatestRunService greatestRunService;

    @PostMapping(value = "create")
    public Team createTeam(@RequestBody Team team) {
        addTeamBySportService.init(Map.of("team", team));
        return addTeamBySportService.execute();
    }

    @PostMapping(value = "addToSeason")
    public List<Map> addTeamToSeason(@RequestParam int seasonId, @RequestParam int teamId) {
        addTeamToSeasonService.init(Map.of("season", seasonId, "team", teamId));
        return addTeamToSeasonService.execute();
    }

    @GetMapping(value = "getBiggestWin")
    public Game getBiggestWin(@RequestParam int teamId, @RequestParam Optional<Boolean> homeGames,
            @RequestParam Optional<Boolean> awayGames,
            @RequestParam Optional<Date> startDate,
            @RequestParam Optional<Date> endDate,
            @RequestParam Optional<Integer> startRound,
            @RequestParam Optional<Integer> endRound){
        
        FilterMetaData metaData = new FilterMetaData();
        metaData.setAwayGames(awayGames.orElse(true));
        metaData.setHomeGames(homeGames.orElse(true));
        metaData.setStartDate(startDate.orElse(MIN_DATE));
        metaData.setEndDate(endDate.orElse(MAX_DATE));
        metaData.setStartRound(startRound.orElse(Integer.MIN_VALUE));
        metaData.setEndRound(endRound.orElse(Integer.MAX_VALUE));
        listGreatestWinByTeamService.init(Map.of("teamId", teamId, "metaData", metaData));
        return listGreatestWinByTeamService.execute();
    }

    @GetMapping(value = "getBiggestLoss")
    public Game getBiggestLoss(@RequestParam int teamId, @RequestParam Optional<Boolean> homeGames,
            @RequestParam Optional<Boolean> awayGames,
            @RequestParam Optional<Date> startDate,
            @RequestParam Optional<Date> endDate,
            @RequestParam Optional<Integer> startRound,
            @RequestParam Optional<Integer> endRound){
        
        FilterMetaData metaData = new FilterMetaData();
        metaData.setAwayGames(awayGames.orElse(true));
        metaData.setHomeGames(homeGames.orElse(true));
        metaData.setStartDate(startDate.orElse(MIN_DATE));
        metaData.setEndDate(endDate.orElse(MAX_DATE));
        metaData.setStartRound(startRound.orElse(Integer.MIN_VALUE));
        metaData.setEndRound(endRound.orElse(Integer.MAX_VALUE));
        listGreatestLossByTeamService.init(Map.of("teamId", teamId, "metaData", metaData));
        return listGreatestLossByTeamService.execute();
    }

    @GetMapping(value = "greatestWinStreak")
    public Map getGreatestWinStreak(@RequestParam int teamId,
            @RequestParam Optional<Date> startDate,
            @RequestParam Optional<Date> endDate) {
        greatestRunService.init(Map.of("teamId", teamId, "gameResult", GameResult.WIN,
                "startDate", startDate.orElse(MIN_DATE), "endDate", endDate.orElse(MAX_DATE)));
        return greatestRunService.execute();
    }

    @GetMapping(value = "greatestLossStreak")
    public Map getGreatestLossStreak(@RequestParam int teamId,
            @RequestParam Optional<Date> startDate,
            @RequestParam Optional<Date> endDate) {
        greatestRunService.init(Map.of("teamId", teamId, "gameResult", GameResult.LOSS,
                "startDate", startDate.orElse(MIN_DATE), "endDate", endDate.orElse(MAX_DATE)));
        return greatestRunService.execute();
    }

    @GetMapping(value = "greatestTieStreak")
    public Map getGreatestTieStreak(@RequestParam int teamId,
            @RequestParam Optional<Date> startDate,
            @RequestParam Optional<Date> endDate) {
        greatestRunService.init(Map.of("teamId", teamId, "gameResult", GameResult.TIE,
                "startDate", startDate.orElse(MIN_DATE), "endDate", endDate.orElse(MAX_DATE)));
        return greatestRunService.execute();
    }
}
