package se.hig.projekt.resource;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.hig.projekt.business.add.AddNewSeasonService;
import se.hig.projekt.business.list.ListAllSeasonsByLeagueService;
import se.hig.projekt.domain.Season;

/**
 * Resource som hanterar säsong operationer.
 * 
 * @author Agila Akrobater
 * @version 2021-05-10
 */
@RestController
@RequestMapping(value = "season")
public class SeasonResource {

    @Autowired
    private AddNewSeasonService addNewSeasonByLeague;

    @Autowired
    private ListAllSeasonsByLeagueService listAllSeasonsByLeagueService;

    @GetMapping(value = "listByLeague")
    public List<Map> getListSeason(@RequestParam int leagueId) {
        listAllSeasonsByLeagueService.init(Map.of("id", leagueId));
        return listAllSeasonsByLeagueService.execute();
    }

    @PostMapping(value = "create")
    public Season addSeason(@RequestBody Season season) {
        addNewSeasonByLeague.init(Map.of("season", season));

        return addNewSeasonByLeague.execute();
    }
}
