package se.hig.projekt.resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.hig.projekt.business.add.AddLeagueToSportService;
import se.hig.projekt.domain.League;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import se.hig.projekt.business.list.ListAllLeaguesBySportService;

/**
 * Resource som hanterar league operationer.
 * 
 * @author Agila Akrobater
 * @version 2021-04-16
 */
@RestController
@RequestMapping(value = "league")
public class LeagueResource {

    @Autowired
    private AddLeagueToSportService addLeagueBySportService;

    @Autowired
    private ListAllLeaguesBySportService listAllLeaguesService;

    @GetMapping(value = "listBySport")
    public List<Map> getListLeague(@RequestParam int sportId) {
        listAllLeaguesService.init(Map.of("id", sportId));
        return listAllLeaguesService.execute();
    }

    @PostMapping(value = "create")
    public League addLeague(@RequestBody League league) {
        addLeagueBySportService.init(Map.of("league", league));
        return addLeagueBySportService.execute();
    }
}
