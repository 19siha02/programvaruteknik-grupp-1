package se.hig.projekt.resource;

import java.sql.Date;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.hig.projekt.business.add.AddGamesService;
import se.hig.projekt.business.add.AddMetaDataToGameService;
import se.hig.projekt.business.add.AddResultToGameService;
import se.hig.projekt.business.add.AddRoundToSeasonService;
import se.hig.projekt.business.list.ListAllGamesBetweenTeamsService;
import se.hig.projekt.business.list.ListAllGamesByDateService;
import se.hig.projekt.business.list.ListAllGamesByRoundService;
import se.hig.projekt.business.list.ListAllGamesBySeasonService;
import se.hig.projekt.business.list.ListAllGamesByTeamService;
import se.hig.projekt.business.list.ListLostGamesByTeamService;
import se.hig.projekt.business.list.ListDrawGamesByTeamService;
import se.hig.projekt.business.list.ListGreatestWinBetweenTwoTeamsService;
import se.hig.projekt.business.list.ListHomeGamesByTeamService;
import se.hig.projekt.business.list.ListVisitorGamesByTeamService;
import se.hig.projekt.business.list.ListWonGamesByTeamService;
import se.hig.projekt.domain.Game;
import se.hig.projekt.domain.Game.EndState;
import se.hig.projekt.utilities.GameMetaData;
import se.hig.projekt.domain.Round;

/**
 * Resource som hanterar game operationer.
 * 
 * @author Agila Akrobater
 * @version 2021-05-10
 */
@RestController
@RequestMapping(value = "game")
public class GameResource {

    @Autowired
    private ListAllGamesByTeamService listAllGamesByTeamService;

    @Autowired
    private ListHomeGamesByTeamService listHomeGamesByTeamService;

    @Autowired
    private ListVisitorGamesByTeamService listVisitorGamesByTeamService;

    @Autowired
    private ListAllGamesBySeasonService listAllGamesBySeasonService;

    @Autowired
    private ListAllGamesByRoundService listAllGamesByRoundService;

    @Autowired
    private ListAllGamesByDateService listAllGamesByDateService;

    @Autowired
    private ListAllGamesBetweenTeamsService listAllGamesBetweenTeamsService;

    @Autowired
    private ListLostGamesByTeamService listLostGamesByTeamService;

    @Autowired
    private ListDrawGamesByTeamService listDrawGamesByTeamService;

    @Autowired
    private ListWonGamesByTeamService listWonGamesByTeamService;

    @Autowired
    private ListGreatestWinBetweenTwoTeamsService listGreatestWinBetweenTwoTeamsService;

    @Autowired
    public AddGamesService addGamesByRoundService;

    @Autowired
    public AddRoundToSeasonService addRoundToSeasonService;

    @Autowired
    private AddMetaDataToGameService addMetaDataToGameService;

    @Autowired
    private AddResultToGameService addResultToGameService;

    @GetMapping(value = "listHeadToHeadGames")
    public List<Map> listAllGamesHeadToHead(@RequestParam int teamOneId,
            @RequestParam int teamTwoId) {

        listAllGamesBetweenTeamsService.init(Map.of("teamOneId", teamOneId, "teamTwoId", teamTwoId));
        return listAllGamesBetweenTeamsService.execute();
    }

    @GetMapping("listByDate")
    public List<Map> listGameByDate(@RequestParam Date date) {
        listAllGamesByDateService.init(Map.of("date", date));
        return listAllGamesByDateService.execute();
    }

    @GetMapping("listByRoundId")
    public List<Map> getGamesByRound(@RequestParam int roundId) {
        listAllGamesByRoundService.init(Map.of("id", roundId));
        return listAllGamesByRoundService.execute();
    }

    @GetMapping("listBySeasonId")
    public List<Map> getGamesBySeason(@RequestParam int seasonId) {
        listAllGamesBySeasonService.init(Map.of("id", seasonId));
        return listAllGamesBySeasonService.execute();
    }

    @GetMapping(value = "listByTeamId")
    public List<Map> getGamesByTeam(@RequestParam int teamId) {
        listAllGamesByTeamService.init(Map.of("id", teamId));
        return listAllGamesByTeamService.execute();
    }

    @GetMapping(value = "listHomeGamesByTeamId")
    public List<Map> getHomeGamesByTeam(@RequestParam int teamId) {
        listHomeGamesByTeamService.init(Map.of("id", teamId));
        return listHomeGamesByTeamService.execute();
    }

    @GetMapping(value = "listVisitorGamesByTeamId")
    public List<Map> getVisitorGamesByTeam(@RequestParam int teamId) {
        listVisitorGamesByTeamService.init(Map.of("id", teamId));
        return listVisitorGamesByTeamService.execute();
    }

    @GetMapping(value = "listLostGamesByTeamId")
    public List<Map> getLostGamesByTeam(@RequestParam int teamId) {
        listLostGamesByTeamService.init(Map.of("id", teamId));
        return listLostGamesByTeamService.execute();
    }

    @GetMapping(value = "listDrawGamesByTeamId")
    public List<Map> getDrawGamesByTeam(@RequestParam int teamId) {
        listDrawGamesByTeamService.init(Map.of("id", teamId));
        return listDrawGamesByTeamService.execute();
    }

    @GetMapping(value = "listWonGamesByTeamId")
    public List<Map> getWonGamesByTeam(@RequestParam int teamId) {
        listWonGamesByTeamService.init(Map.of("id", teamId));
        return listWonGamesByTeamService.execute();
    }

    @GetMapping(value = "listGreatestWinBetweenTwoTeams")
    public List<Map> getGame(
            @RequestParam int teamOneId,
            @RequestParam int teamTwoId) {
        listGreatestWinBetweenTwoTeamsService.init(Map.of("teamOneId", teamOneId, "teamTwoId", teamTwoId));
        return listGreatestWinBetweenTwoTeamsService.execute();
    }

    @Transactional(rollbackFor = {ConstraintViolationException.class})
    @PostMapping(value = "addGames")
    public List<Game> addGamesByRound(
            @RequestParam int seasonId,
            @RequestParam int roundNo,
            @RequestBody List<Game> listOfGames) {
        
        addRoundToSeasonService.init(Map.of("roundNo", roundNo, "seasonId", seasonId));
        Round roundEntity = addRoundToSeasonService.execute();

        listOfGames.forEach(game -> game.setRound(roundEntity));

        addGamesByRoundService.init(Map.of("games", listOfGames));
        return addGamesByRoundService.execute();

    }

    @PutMapping(value = "addMetaData")
    public Game addMetaData(@RequestBody GameMetaData metaData, @RequestParam int gameId) {

        addMetaDataToGameService.init(Map.of("id", gameId, "metaData", metaData));
        return addMetaDataToGameService.execute();
    }

    @PutMapping(value = {"addResult"})
    public Game addResult(@RequestParam int gameId, @RequestParam int homeScore,
            @RequestParam int visitorScore, @RequestParam EndState endState) {

        addResultToGameService.init(Map.of(
                "id", gameId,
                "homeScore", homeScore,
                "visitorScore", visitorScore,
                "endState", endState));
        return addResultToGameService.execute();
    }

}
