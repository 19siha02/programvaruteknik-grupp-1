/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.hig.projekt.resource;

import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.hig.projekt.business.list.MultipleSeasonTableService;
import se.hig.projekt.business.list.SingleSeasonTableService;
import se.hig.projekt.utilities.Table;
import se.hig.projekt.utilities.FilterMetaData;

/**
 * Resource som skapar tabeller.
 * 
 * @author Agila Akrobater
 * @version 2021-05-10
 */
@RestController
@RequestMapping(value = "table")
public class TableResource {
    
    private final Date MIN_DATE = Date.valueOf("0000-01-01");
    private final Date MAX_DATE = Date.valueOf("4628-01-01");
    
    @Autowired
    private SingleSeasonTableService singleSeasonTableService;
    
    @Autowired
    private MultipleSeasonTableService multipleSeasonTableService;
    
   
    
    
    @GetMapping("getTable")
    public Table getSingleSeasonTable(@RequestParam int seasonId){
        FilterMetaData metaData = new FilterMetaData();
        metaData.setAwayGames(true);
        metaData.setHomeGames(true);
        metaData.setStartDate(MIN_DATE);
        metaData.setEndDate(MAX_DATE);
        metaData.setStartRound(Integer.MIN_VALUE);
        metaData.setEndRound(Integer.MAX_VALUE);
        singleSeasonTableService.init(Map.of("seasonId", seasonId, "metaData", metaData));
        return singleSeasonTableService.execute();
    }
    
    @GetMapping("getTable/multipleSeasons")
    public Table getMultipleSeasonTable(@RequestParam List<Integer> seasonIds,
            @RequestParam Optional<Boolean> homeGames,
            @RequestParam Optional<Boolean> awayGames,
            @RequestParam Optional<Date> startDate,
            @RequestParam Optional<Date> endDate,
            @RequestParam Optional<Integer> startRound,
            @RequestParam Optional<Integer> endRound){
        
        FilterMetaData metaData = new FilterMetaData();
        metaData.setAwayGames(awayGames.orElse(true));
        metaData.setHomeGames(homeGames.orElse(true));
        metaData.setStartDate(startDate.orElse(MIN_DATE));
        metaData.setEndDate(endDate.orElse(MAX_DATE));
        metaData.setStartRound(startRound.orElse(Integer.MIN_VALUE));
        metaData.setEndRound(endRound.orElse(Integer.MAX_VALUE));
        multipleSeasonTableService.init(Map.of("seasonIdList", seasonIds, "filterMetaData", metaData));
        
        return multipleSeasonTableService.execute();
    }
    
    @GetMapping("getTable/getWithFilter/")
    public Table getHomeOrAwayTable(@RequestParam int seasonId, 
            @RequestParam Optional<Boolean> homeGames,
            @RequestParam Optional<Boolean> awayGames,
            @RequestParam Optional<Date> startDate,
            @RequestParam Optional<Date> endDate,
            @RequestParam Optional<Integer> startRound,
            @RequestParam Optional<Integer> endRound){
        
        FilterMetaData metaData = new FilterMetaData();
        metaData.setAwayGames(awayGames.orElse(true));
        metaData.setHomeGames(homeGames.orElse(true));
        metaData.setStartDate(startDate.orElse(MIN_DATE));
        metaData.setEndDate(endDate.orElse(MAX_DATE));
        metaData.setStartRound(startRound.orElse(Integer.MIN_VALUE));
        metaData.setEndRound(endRound.orElse(Integer.MAX_VALUE));
        singleSeasonTableService.init(Map.of("seasonId", seasonId, "metaData", metaData));
        
        return singleSeasonTableService.execute();
                
    }
    
    
    
    
}
