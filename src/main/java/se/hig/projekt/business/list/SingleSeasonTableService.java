package se.hig.projekt.business.list;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.SeasonDao;
import se.hig.projekt.domain.Season;
import se.hig.projekt.utilities.Table;
import se.hig.projekt.utilities.FilterMetaData;

/**
 * En tabell för en säsong.
 * 
 * @author Agila Akrobater
 * @version 2021-04-27
 */
@Service
public class SingleSeasonTableService implements IService<Table, Map<String, Integer>> {

    @Autowired
    private SeasonDao seasonDao;
    private int seasonId;
    private Table table;
    private FilterMetaData metaData;

    @Override
    public void init(Map map) {
        this.seasonId = (int) map.get("seasonId");
        this.metaData = (FilterMetaData) map.get("metaData");
    }

    @Override
    public Table execute() {
        List<Season> seasonList = new ArrayList();
        seasonList.add(seasonDao.findByIdOrThrow(seasonId));
        table = new Table(metaData);
        table.setSeasonList(seasonList);
        table.buildTable();
        return table;
    }

}
