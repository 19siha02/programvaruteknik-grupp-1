package se.hig.projekt.business.list;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.SportDao;

/**
 * Lista alla sporter.
 * 
 * @author Agila Akrobater
 * @version 2021-04-27
 */

@Service
public class ListAllSportsService implements IService<List<Map>, Map<String, Integer>> {

    
    @Autowired
    SportDao sportDao;
    
    @Override
    public void init(Map<String, Integer> map) {
        
    }

    @Override
    public List<Map> execute() {
        return sportDao.findAll().stream()
                .map(sport -> Map.of(
                        "name", sport.getName(), 
                        "id", sport.getId()))
                .collect(Collectors.toList());
    }

}
