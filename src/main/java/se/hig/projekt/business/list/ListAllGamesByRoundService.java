package se.hig.projekt.business.list;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.RoundDao;
import se.hig.projekt.domain.Round;

/**
 * Lista alla matcher för en specifik runda.
 * 
 * @author Agila Akrobater
 * @version 2021-05-03
 */

@Service
public class ListAllGamesByRoundService implements IService<List<Map>, Map<String, Integer>> {
    
    @Autowired
    private RoundDao roundDao;
    
    private int roundId;
    
    @Override
    public void init(Map<String, Integer> map) {
        roundId = map.get("id");
    }

    @Override
    public List<Map> execute() {
        Round round = roundDao.findByIdOrThrow(roundId);
        return round.getGames().stream()
                .map(game -> Map.of(
                        "id", game.getId(), 
                        "visitor", game.getVisitorTeam().getName(),
                        "home", game.getHomeTeam().getName(),
                        "result", game.getHomeScore() + " - " + game.getVisitorScore() + 
                        " " + game.getEndState().toString()))
                .collect(Collectors.toList());
    }
    
 
    

}
