package se.hig.projekt.business.list;

import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.GameDao;

/**
 * Listar alla matcher beroende på datum.
 * 
 * @author Agila Akrobater
 * @version 2021-04-27
 */
@Service
public class ListAllGamesByDateService implements IService<List<Map>, Map<String, Date>> {

    @Autowired
    GameDao gameDao;

    @Temporal(TemporalType.DATE)
    Date date;

    @Override
    public void init(Map<String, Date> map) {
        date = map.get("date");  
    }

    @Override
    public List<Map> execute() {
        return gameDao.findAll().stream()
                .filter(game -> game.getGameDate().toLocalDate().equals(date.toLocalDate()))
                .map(game -> Map.of(
                "id", game.getId(),
                "homeTeam", game.getHomeTeam().getName(),
                "visitorTeam", game.getVisitorTeam().getName()))
                .collect(Collectors.toList());
    }

}
