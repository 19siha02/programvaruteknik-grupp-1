package se.hig.projekt.business.list;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.TeamDao;
import se.hig.projekt.domain.Team;

/**
 * Lista bortamatcher för ett lag.
 * 
 * @author Agila Akrobater
 * @ver 2021-04-27
 */
@Service
public class ListVisitorGamesByTeamService implements IService<List<Map>, Map<String, Integer>> {

    @Autowired
    private TeamDao teamDao;
    
    private int teamId;
    
    @Override
    public void init(Map<String, Integer> map) {
        this.teamId = map.get("id");
    }

    @Override
    public List<Map> execute() {
       Team team = teamDao.findByIdOrThrow(teamId);
        return team.getAwayGames().stream()
                .map(game -> Map.of(
                        "id", game.getId(), 
                        "visitor", game.getVisitorTeam().getName(), 
                        "home", game.getHomeTeam().getName()))
                .collect(Collectors.toList());
    }
}
