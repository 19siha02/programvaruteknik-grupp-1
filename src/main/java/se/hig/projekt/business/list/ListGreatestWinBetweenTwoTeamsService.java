package se.hig.projekt.business.list;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.GameDao;
import se.hig.projekt.domain.Game;

/**
 * Lista största vinsten emellan två lag.
 * 
 * @author Agila Akrobater
 * @version 2021-05-10
 */
@Service
public class ListGreatestWinBetweenTwoTeamsService implements IService<List<Map>, Map<String, Integer>> {

    @Autowired
    private GameDao gameDao;

    private int teamOneId;
    private int teamTwoId;
    
    int highestScoreDifference = 0;

    List<Game> gamesWithBiggestDiff = new ArrayList();
    
    @Override
    public void init(Map<String, Integer> map) {
        teamOneId = map.get("teamOneId");
        teamTwoId = map.get("teamTwoId");
    }

    @Override
    public List<Map> execute() {
        
        List<Game> games = allGamesBetweenTwoTeams();
        games.forEach(g -> biggestScoreDiffInGame(g));
        
        return gamesWithBiggestDiff.stream().map(game -> Map.of("id", game.getId(),
                "homeTeam", game.getHomeTeam().getName(),
                "visitorTeam", game.getVisitorTeam().getName(),
                "Game date: ", game.getGameDate(),
                "Score: ", game.getHomeScore() + " - " + game.getVisitorScore())).collect(Collectors.toList());
    }

    private List<Game> allGamesBetweenTwoTeams() {
        List<Game> games = gameDao.findAll().stream().filter(
                game -> (game.getHomeTeam().getId() == teamOneId && game.getVisitorTeam().getId() == teamTwoId)
                        || (game.getHomeTeam().getId() == teamTwoId
                                && game.getVisitorTeam().getId() == teamOneId)).collect(Collectors.toList());
        return games;
    }

    private void biggestScoreDiffInGame(Game game) {
        int currentScoreDifference = Math.abs(game.getVisitorScore() - game.getHomeScore());
        if (currentScoreDifference > highestScoreDifference) {
            gamesWithBiggestDiff.clear();
            gamesWithBiggestDiff.add(game);
            highestScoreDifference = currentScoreDifference;
        }
        else if(currentScoreDifference == highestScoreDifference )
            gamesWithBiggestDiff.add(game);
    }
}
