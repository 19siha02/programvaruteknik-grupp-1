package se.hig.projekt.business.list;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.SportDao;
import se.hig.projekt.domain.Sport;


/**
 * Lista alla ligor för en sport.
 * 
 * @author Agila Akrobater
 * @version 2021-04-19
 */
@Service
public class ListAllLeaguesBySportService implements IService<List<Map>, Map<String, Integer>> {
    
    
    @Autowired
    private SportDao sportDao;
    
    private int sportId;
    
    @Override
    public List<Map> execute() {
        Sport sport = sportDao.findByIdOrThrow(sportId);
        return sport.getLeagues().stream()
                .map(league -> Map.of(
                        "name", league.getName(), 
                        "id", league.getId()))
                .collect(Collectors.toList()); 
    }

    @Override
    public void init(Map<String, Integer> map) {
        sportId = map.get("id");
    }
}
