package se.hig.projekt.business.list;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.TeamDao;
import se.hig.projekt.domain.Game;
import se.hig.projekt.domain.Team;
import se.hig.projekt.utilities.Filter;
import se.hig.projekt.utilities.GameResult;

/**
 * Lista största streak:en för ett lag.
 * 
 * @author Agila Akrobater
 * @version 2021-04-27
 */
@Service
public class ListGreatestRunService implements IService<Map, Map> {

    @Autowired
    private TeamDao teamDao;

    private Team team;

    private GameResult gameResult;
    private Date startDate;
    private Date endDate;

    @Override
    public void init(Map map) {
        team = teamDao.findByIdOrThrow((int) map.get("teamId"));
        gameResult = (GameResult) map.get("gameResult");
        startDate = (Date) map.get("startDate");
        endDate = (Date) map.get("endDate");
        System.out.println(endDate);
    }

    //TODO glöm inte att vi har alla samtliga spel även dom som ej har spelats
    //TODO filter för intervall
    @Override
    public Map execute() {
        List<Game> gameList = team.getAllGames();
        Collections.sort(gameList);
        Stream<Game> gameStream = gameList.stream();
        Predicate<Game> predicate = null;

        switch (gameResult) {
            case WIN:
                predicate = (Game g) -> {
                    return g.getWinner() != null && g.getWinner().getId() == team.getId();
                };
                break;
            case LOSS:
                predicate = (Game g) -> {
                    return g.getWinner() != null && g.getWinner().getId() != team.getId();
                };
                break;
            case TIE:
                predicate = (Game g) -> {
                    return g.getWinner() == null;
                };
                break;
        }
       
        gameStream = Filter.dateFilter(gameStream, startDate, endDate);
        
        gameList = getGreatestStreak(gameStream, predicate);
        
        if(gameList.isEmpty())
        return Map.of("Ingen streak funnen", "kul");

        return Map.of("streakStart", gameList.get(0).getGameDate(),
                "streakEnd", gameList.get(gameList.size() - 1).getGameDate(),
                "streakLength", gameList.size());
    }

    private List<Game> getGreatestStreak(Stream<Game> gameList, Predicate<Game> predicate) {
        List<Game> currentStreak = new ArrayList<>();
        List<Game> maxStreak = new ArrayList<>();
        gameList.forEach(game -> {
            checkIfGameInStreak(predicate, game, currentStreak, maxStreak);
        });

        return maxStreak;
    }

    private void checkIfGameInStreak(Predicate<Game> predicate, Game g, List<Game> currentStreak, List<Game> maxStreak) {
        if (predicate.test(g)) {
            currentStreak.add(g);
            if (currentStreak.size() > maxStreak.size()) {
                maxStreak.clear();
                maxStreak.addAll(currentStreak);
            }
        } else {
            currentStreak.clear();
        }
    }
}
