package se.hig.projekt.business.list;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.LeagueDao;
import se.hig.projekt.domain.League;

/**
 * Lista alla säsonger för en liga.
 * 
 * @author Agila Akrobater
 * @version 2021-04-26
 */
@Service
public class ListAllSeasonsByLeagueService implements IService<List<Map>, Map<String, Integer>> {
    
    @Autowired
    private LeagueDao leagueDao;
    
    private int leagueId;
    
    @Override
    public void init(Map<String, Integer> map) {
        leagueId = map.get("id");
    }
    @Override
    public List<Map> execute() {
        League league = leagueDao.findByIdOrThrow(leagueId);
        return league.getSeasons().stream()
                .map(season -> Map.of(
                        "name", season.getName(), 
                        "id", season.getId()))
                .collect(Collectors.toList());
    }
}
