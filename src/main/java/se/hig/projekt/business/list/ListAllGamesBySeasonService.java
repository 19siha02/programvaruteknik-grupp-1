package se.hig.projekt.business.list;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.GameDao;

/**
 * Lista alla matcher för en säsong.
 * 
 * @author Agila Akrobater
 * @version 2021-05-03
 */

@Service
public class ListAllGamesBySeasonService implements IService<List<Map>, Map<String, Integer>>{

    @Autowired
    private GameDao gameDao;
    
    private int seasonId;
    
    @Override
    public void init(Map<String, Integer> map) {
        seasonId = map.get("id");
    }

    @Override
    public List<Map> execute() {
        return findGamesBySeason(seasonId);
    }
    
    private List<Map> findGamesBySeason(int seasonId){
        return gameDao.findAll().stream()
                .filter(game -> game.getRound().getSeason().getId() == seasonId)
                .map(game -> Map.of(
                        "id", game.getId(), 
                        "visitor", game.getVisitorTeam().getName(),
                        "home", game.getHomeTeam().getName(),
                        "result", game.getHomeScore() + " - " + game.getVisitorScore() + 
                        " " + game.getEndState().toString()))
                .collect(Collectors.toList());
    }
}
