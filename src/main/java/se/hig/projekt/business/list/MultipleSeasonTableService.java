package se.hig.projekt.business.list;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.hig.projekt.business.IService;
import se.hig.projekt.dao.SeasonDao;
import se.hig.projekt.domain.Season;
import se.hig.projekt.utilities.FilterMetaData;
import se.hig.projekt.utilities.Table;

/**
 * Tabel över flera säsonger.
 * 
 * @author Agila Akrobater
 * @version 2021-04-27
 */
@Service
public class MultipleSeasonTableService implements IService<Table, Map>{

    @Autowired
    private SeasonDao seasonDao;
    
    List<Season> seasonList;
    FilterMetaData metaData;
    Table table;
    
    @Override
    public void init(Map map) {
        
        seasonList = seasonDao.findAllById((List<Integer>)map.get("seasonIdList"));
        metaData = (FilterMetaData) map.get("filterMetaData");
    }

    @Override
    public Table execute() {
        table = new Table(metaData);
        table.setSeasonList(seasonList);
        table.buildTable();
        return table;
    }
    
}
