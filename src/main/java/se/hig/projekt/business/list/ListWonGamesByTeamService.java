package se.hig.projekt.business.list;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.TeamDao;
import se.hig.projekt.domain.Team;

/**
 * Lista vinster för ett lag.
 * 
 * @author Agila Akrobater
 * @version 2021-05-10
 */
@Service
public class ListWonGamesByTeamService implements IService<List<Map>, Map<String, Integer>>{

    @Autowired
    private TeamDao teamDao; 
    
    private int teamId;
    
    @Override
    public void init(Map<String, Integer> map) {
        this.teamId = map.get("id");
    }

    @Override
    public List<Map> execute() {
       Team team = teamDao.findByIdOrThrow(teamId);
       return team.getAllGames().stream()
               .filter(game -> (
                       teamId == game.getVisitorTeam().getId() && game.getVisitorScore() > game.getHomeScore() 
                       || teamId == game.getHomeTeam().getId() && game.getHomeScore() > game.getVisitorScore()))
               .map(game -> Map.of(
               "id", game.getId(), 
               "visitor", game.getVisitorTeam().getName(),
               "visitorScore", game.getVisitorScore(),
               "home", game.getHomeTeam().getName(),
               "homeScore", game.getHomeScore()))
               .collect(Collectors.toList());                    
    }
    
}
