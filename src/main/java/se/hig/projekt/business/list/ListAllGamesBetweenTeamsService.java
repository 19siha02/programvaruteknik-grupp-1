package se.hig.projekt.business.list;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.GameDao;
import se.hig.projekt.domain.Game;

/**
 * Listar all matcher mellan två lag. 
 * 
 * @author Agila Akrobater
 * @version 2021-04-27
 */
@Service
public class ListAllGamesBetweenTeamsService implements IService<List<Map>, Map<String, Integer>>{
    @Autowired
    private GameDao gameDao;

    private int teamOneId, teamTwoId;
   

    @Override
    public void init(Map<String, Integer> map) {
        teamOneId = map.get("teamOneId");
        teamTwoId = map.get("teamTwoId");
        
    }

    @Override
    public List<Map> execute() {
        return streamOfCombinedGames()
                .map(game -> Map.of(
                "gameId", game.getId(),
                "homeTeam", game.getHomeTeam().getName(),
                "visitorTeam", game.getVisitorTeam().getName(),
                "season", game.getRound().getSeason().getName(),
                "round", game.getRound().getRoundNo(),
                "league", game.getRound().getSeason().getLeague().getName(),
                "result", game.getHomeScore() + " - " + game.getVisitorScore() + 
                        " " + game.getEndState().toString()))
                .collect(Collectors.toList());
    }
    
    private Stream<Game> streamOfCombinedGames(){
        return gameDao.findAll().stream()
                .filter(game -> (game.getHomeTeam().getId()==teamOneId 
                        && game.getVisitorTeam().getId()==teamTwoId)
                || (game.getHomeTeam().getId()==teamTwoId 
                        && game.getVisitorTeam().getId()==teamOneId));
    }
}
