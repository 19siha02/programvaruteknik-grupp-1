package se.hig.projekt.business.list;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.TeamDao;
import se.hig.projekt.utilities.Filter;
import se.hig.projekt.domain.Game;
import se.hig.projekt.utilities.FilterMetaData;
import se.hig.projekt.domain.Team;

/**
 * Lista största förlusten för ett lag.
 * 
 * @author Agila Akrobater
 * @version 2021-04-27
 */
@Service
public class ListGreatestLossByTeamService implements IService<Game, Map> {

    @Autowired
    private TeamDao teamDao;

    private Game biggestLoss; 
    private int teamId;
    private FilterMetaData tableMetaData;
    

    @Override
    public void init(Map map) {
        teamId = (int) map.get("teamId");
        tableMetaData = (FilterMetaData) map.get("metaData");
    }

    @Override
    public Game execute() {

        biggestLoss = null;
        Team team = teamDao.findByIdOrThrow(teamId);
        
        List<Game> gamesList = team.getAllGames()
                .stream()
                .filter(game -> game.getWinner() != null && game.getWinner().getId() != teamId)
                .collect(Collectors.toList());
        
        Stream<Game> gameStream = gamesList.stream();

        if(tableMetaData.getStartDate()!= null && tableMetaData.getEndDate() != null){
            gameStream = Filter.dateFilter(gameStream, tableMetaData.getStartDate(), tableMetaData.getEndDate());
        }
        
        if (tableMetaData.getStartRound()!= null && tableMetaData.getEndRound() != null ) {
            gameStream = Filter.roundFilter(gameStream, tableMetaData.getStartRound(), tableMetaData.getEndRound()) ;
        }
        
        gamesList = gameStream.collect(Collectors.toList());
        if(!gamesList.isEmpty()) {
            biggestLoss = gamesList.get(0);
        }
        
        gamesList.forEach(game -> biggestWin(game));
        
        return biggestLoss;
    }
    
    
    private void biggestWin(Game game) {
                
         if (Math.abs(game.getHomeScore() - game.getVisitorScore())
                    > Math.abs(biggestLoss.getHomeScore() - biggestLoss.getVisitorScore())) {
                        biggestLoss = game;
                    }
       
    }
}
