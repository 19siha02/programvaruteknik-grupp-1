package se.hig.projekt.business.add;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.GameDao;
import se.hig.projekt.domain.Game;
import se.hig.projekt.utilities.GameMetaData;

/**
 * Uppdaterar ett spel med en viss {@link GameMetaData}. 
 *
 * @author Agila Akrobater
 * @version 2021-04-27
 */
@Service
public class AddMetaDataToGameService implements IService<Game, Map>{

    @Autowired
    private GameDao gameDao;
     
    private Map<String,GameMetaData> metaDataMap;
    private Game game;

    @Override
    public void init(Map map) {   
        game = gameDao.findByIdOrThrow((int) map.get("id"));
        this.metaDataMap =  map;
    }

    @Override
    public Game execute() {
        game.setGameMetaData(metaDataMap); 
        return gameDao.save(game);
    }
}
