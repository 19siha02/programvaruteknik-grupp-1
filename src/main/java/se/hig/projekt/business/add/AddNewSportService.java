package se.hig.projekt.business.add;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.SportDao;
import se.hig.projekt.domain.Sport;

/**
 * 
 * Lägger till en ny sport.
 * 
 * @author Agila Akrobater
 * @version 2021-04-16
 */

@Service
public class AddNewSportService implements IService<Sport, Map<String, Sport>> {

    @Autowired
    private SportDao sportDao;
    private Sport sport;

    @Override
    public Sport execute() {
        if (sport.getName() == null) {
            throw new NullPointerException("Name cannot be null.");
        }
        if (sport.getName().isEmpty() || sport.getName().length() < 3 || sport.getName().length() > 45) {
            throw new IllegalArgumentException("Illegal argument, name length needs to be between 3 - 45");
        }
        Map.of("id", sportDao.save(sport).getId());
        return sport;
    }

    @Override
    public void init(Map<String, Sport> map) {
        sport = map.get("sport");
    }

}
