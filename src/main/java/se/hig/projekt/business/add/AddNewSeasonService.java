package se.hig.projekt.business.add;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.SeasonDao;
import se.hig.projekt.domain.Season;

/**
 * Lägger till en ny säsong.
 * 
 * @author Agila Akrobater
 * @version 2021-04-27
 */
@Service
public class AddNewSeasonService implements IService<Season, Map<String, Season>>{

    @Autowired
    private SeasonDao seasonDao;
    
    private Season season;
    
    @Override
    public void init(Map<String, Season> map) {
        season = map.get("season");
    }

    @Override
    public Season execute() {
        checkSeasonIntegrity();
        Map.of("id", seasonDao.save(season).getId());    
        return season;
    }

    private void checkSeasonIntegrity() throws IllegalArgumentException, NullPointerException {
        if (season.getName() == null || season.getLeague() == null) {
            throw new NullPointerException("Name or League cannot be null.");
        }
        if (season.getName().length() < 3 || season.getName().length() >= 45) {
            throw new IllegalArgumentException("Illegal argument, check the manual!");
        }
    }

}
