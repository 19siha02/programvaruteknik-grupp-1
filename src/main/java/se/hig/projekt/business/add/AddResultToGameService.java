package se.hig.projekt.business.add;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.GameDao;
import se.hig.projekt.domain.Game;
import se.hig.projekt.domain.Game.EndState;

/**
 * Lägger till/uppdaterar ett resultat för en specifik match.
 * 
 * @author Agila Akrobater
 * @version 2021-04-27
 */
@Service
public class AddResultToGameService implements IService<Game, Map>{

    @Autowired
    private GameDao gameDao;
    
    private Game game;
    private int homeScore, visitorScore;
    private EndState endState;
    
    @Override
    public void init(Map map) {
        game = gameDao.findByIdOrThrow((int)(map.get("id")));
        homeScore = (int)map.get("homeScore");
        visitorScore = (int)map.get("visitorScore");
        endState = (EndState)map.get("endState");
    }

    @Override
    public Game execute() {
        game.setHomeScore(homeScore);
        game.setVisitorScore(visitorScore);
        game.setEndState(endState);
        
        return gameDao.save(game);
    }

}
