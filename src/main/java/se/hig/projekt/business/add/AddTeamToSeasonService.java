package se.hig.projekt.business.add;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.SeasonDao;
import se.hig.projekt.dao.TeamDao;
import se.hig.projekt.domain.Season;
import se.hig.projekt.domain.Team;

/**
 * Lägger till ett lag till en säsong.
 * 
 * @author Agila Akrobater
 * @version 2021-04-27
 */

@Service
public class AddTeamToSeasonService implements IService<List<Map>, Map<String, Integer>>{
    
    @Autowired
    private SeasonDao seasonDao;
    @Autowired
    private TeamDao teamDao;
    private Season season;
    private Team team;
    

    @Override
    public void init(Map<String, Integer> map) {
        season = seasonDao.findByIdOrThrow(map.get("season"));
        team = teamDao.findByIdOrThrow(map.get("team"));
    }
    

    @Override
    public List<Map> execute() { 
        
        if (team.getSeasons().contains(season)) {
            throw new RuntimeException("Laget tillhör redan den säsongen.");
        }
        season.getTeams().forEach(t -> {
            if(t.getName().equals(team.getName()))
                throw new IllegalArgumentException("Dublettnamn");
        });
        return teamDao.updateSeasonList(season, team);
    }

    
    
}
