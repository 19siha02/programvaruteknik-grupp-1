package se.hig.projekt.business.add;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.TeamDao;
import se.hig.projekt.domain.Team;

/**
 * Lägger till ett lag till en sport.
 * 
 * @author Agila Akrobater
 * @version 2021-04-26
 */
@Service
public class AddTeamBySportIdService implements IService<Team, Map<String, Team>> {

    @Autowired
    private TeamDao teamDao;
    private Team team;

    @Override
    public void init(Map<String, Team> map) {
        team = map.get("team");
    }

    @Override
    public Team execute() {
        checkIfNameIsCorrect(team);
        checkIfSportIsNull(team);
        findDuplicateTeamByNameAndThrow(team.getName());
        teamDao.save(team);
        return team;
    }

    private void findDuplicateTeamByNameAndThrow(String name) {
        List<Team> result = teamDao.findAll().stream()
                .filter(team -> team.getName().equals(name))
                .collect(Collectors.toList());
        if (result.size() > 0) {
            throw new IllegalArgumentException("Team already exists");
        }
    }

    private void checkIfNameIsCorrect(Team team) {
        if(team.getName() == null){
            throw new NullPointerException("Name can't be null");
        }
        if(team.getName().length() < 3 || team.getName().length() > 45){
            throw new IllegalArgumentException("Team name needs to be between 3 - 45.");
        }
    }

    private void checkIfSportIsNull(Team team) {
        if(team.getSport() == null){
            throw new NullPointerException("Team needs to be associated with a sport.");
        }
        
    }

}
