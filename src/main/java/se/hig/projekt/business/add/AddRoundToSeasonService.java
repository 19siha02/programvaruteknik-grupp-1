package se.hig.projekt.business.add;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.RoundDao;
import se.hig.projekt.dao.SeasonDao;
import se.hig.projekt.domain.Round;
import se.hig.projekt.domain.Season;

/**
 * Lägger till en runda till en specifik säsong.
 * 
 * @author Agila Akrobater
 * @version 2021-04-27
 */
@Service
public class AddRoundToSeasonService implements IService<Round, Map<String, Integer>>{
    
    @Autowired
    private RoundDao roundDao;
    
    @Autowired
    private SeasonDao seasonDao;
    
    private int roundNo;
    private int seasonId;
    
    
    @Override
    public void init(Map<String, Integer> map) {
        roundNo = map.get("roundNo");
        seasonId = map.get("seasonId");
        
    }

    @Override
    public Round execute() {
        Season season = seasonDao.findByIdOrThrow(seasonId);
        checkValidRound(season);
        
        Round round = new Round(roundNo, season);
        season.getRounds().forEach(r -> {   
            if(r.getRoundNo()==round.getRoundNo())
                throw new DuplicateKeyException("Rundan finns redan för säsongen");
        });
        
        return roundDao.save(round);
    }

    private void checkValidRound(Season season) throws IllegalArgumentException {
        if(roundNo<1){
            throw new IllegalArgumentException("Rundans nummer måste vara större än 0.");
        }
        if(season.getMaxOfRounds()<roundNo){
            throw new IllegalArgumentException("Maxantal för rundor har överstigits");
        }
    }

}
