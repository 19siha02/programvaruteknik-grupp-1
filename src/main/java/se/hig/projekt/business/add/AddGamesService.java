package se.hig.projekt.business.add;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.GameDao;
import se.hig.projekt.domain.Game;

/**
 * Tar in en lista av matcher och sparar till databasen.
 *
 * @author Agila Akrobater
 * @version 2021-04-27
 */
@Service
public class AddGamesService implements IService<List<Game>, Map<String, List<Game>>> {

    @Autowired
    private GameDao gameDao;

    List<Game> listOfGames;

    @Override
    public void init(Map<String, List<Game>> map) {
        listOfGames = map.get("games");
    }

    @Override
    public List<Game> execute() {
        return gameDao.saveAll(listOfGames);
    }
}
