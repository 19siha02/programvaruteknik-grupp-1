package se.hig.projekt.business.add;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import se.hig.projekt.domain.League;
import org.springframework.stereotype.Service;
import se.hig.projekt.business.IService;
import se.hig.projekt.dao.LeagueDao;

/**
 * Lägger till en liga för en specifik sport.
 * 
 * @author Agila Akrobater
 * @version 2021-04-16
 */
@Service
public class AddLeagueToSportService implements IService<League, Map<String, League>> {

    @Autowired
    private LeagueDao leagueDao;
    
    private League league;

    @Override
    public void init(Map<String, League> map) {
       league = map.get("league");
    }

    @Override
    public League execute() {
        if(league.getName() == null || league.getSport() == null) {
            throw new NullPointerException();
        }
        
        if(!correctNameLenght(league) || checkMinValue(league)){
            throw new IllegalArgumentException();
        }
        
        Map.of("id", leagueDao.save(league).getId());
        return league;
    }
    
    private boolean correctNameLenght(League league){
        return (league.getName().length() > 2 && league.getName().length() <= 45);
    }
    
    private boolean checkMinValue(League league){
        return (league.getWinValue() < 0 || league.getTieValue() < 0 || league.getOvertimeWinValue() < 0 || league.getOvertimeLossValue() < 0);
    }


}
