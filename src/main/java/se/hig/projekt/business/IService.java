package se.hig.projekt.business;


/**
 * Strukturen för en service.
 * 
 * @author Agila Akrobater
 * @param <T> retur typen
 * @param <U> in typen
 * 
 */

public interface IService <T, U> {  
    
    /**
     * Det som behövs för att ladda servicen.
     * @param load 
     */
    void init(U load);
   
    /**
     * Det som skall utföras.
     * @return T
     */
    T execute();
    
}
