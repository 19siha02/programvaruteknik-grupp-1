/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.hig.projekt.business;

import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import se.hig.projekt.business.add.AddNewSportService;
import se.hig.projekt.dao.SportDao;
import se.hig.projekt.domain.Sport;

/**
 *
 * @author Simon
 */
//@RunWith(SpringRunner.class)
//@ContextConfiguration(
//classes = { TestConfiguration.class})
public class AddNewSportServiceTest {
    
    @InjectMocks
    private AddNewSportService service;
    @Mock
    private Sport mockedSport;
    @Mock
    private SportDao mockedDao;
    
    private Map map;
    
    
    
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        map = new HashMap<String, Sport>();
        map.put("sport", mockedSport);
        service.init(map);
        when(mockedDao.save(mockedSport)).thenReturn(mockedSport);
        
        when(mockedSport.getId()).thenReturn(1);
       
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createSport method, of class AddSportResource.
     */
    @Test
    public void testCreateSport() {
        when(mockedSport.getName()).thenReturn("Fotboll");
        assertEquals("Fotboll", service.execute().getName());    
    }
    @Test
    public void testCreateSportNull(){
        when(mockedSport.getName()).thenReturn(null);
        assertThrows(NullPointerException.class, () -> service.execute());
    }
    
    @Test
    public void testCreateSportShort(){
        when(mockedSport.getName()).thenReturn("as");
        assertThrows(IllegalArgumentException.class, () -> service.execute());
    }
    
    @Test
    public void testCreateSportLong() {
        when(mockedSport.getName()).thenReturn("gfuaSJCHGVKDJLsabdnökASDHLIEWLYRFGEWVjkbdladsjkgwdfjhlJasödgkgjhDSAFGasldfhjkfwDSEAJLHGHJAsdjfhgHSJGDFHLGJSFD");
        assertThrows(IllegalArgumentException.class, () -> service.execute());
    }
    
    
}
