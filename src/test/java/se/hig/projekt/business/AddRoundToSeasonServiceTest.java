/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.hig.projekt.business;

import se.hig.projekt.business.add.AddRoundToSeasonService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DuplicateKeyException;
import se.hig.projekt.dao.RoundDao;
import se.hig.projekt.dao.SeasonDao;
import se.hig.projekt.domain.Round;
import se.hig.projekt.domain.Season;

/**
 *
 * @author Simon
 */
public class AddRoundToSeasonServiceTest {
    
    @InjectMocks
    private AddRoundToSeasonService service;
    
    @Mock
    private RoundDao roundDao;
    
    @Mock
    private SeasonDao seasonDao;
    
    @Mock 
    private Season season;
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
    

    @Test
    public void testNegativeRoundNo(){
        when(seasonDao.findByIdOrThrow(1)).thenReturn(season);     
        service.init(Map.of("roundNo", -1, "seasonId",1));
        assertThrows(IllegalArgumentException.class, () -> service.execute());
    }
    
    @Test
    public void testMaxRoundNo(){
        service.init(Map.of("roundNo", 3, "seasonId",1));
        when(seasonDao.findByIdOrThrow(1)).thenReturn(season);
        when(season.getMaxOfRounds()).thenReturn(2);
        assertThrows(IllegalArgumentException.class, () -> service.execute());
    }
    
    @Test
    public void testDuplicateRoundNoInSeason(){
        service.init(Map.of("roundNo", 1, "seasonId",1));
        when(seasonDao.findByIdOrThrow(1)).thenReturn(season);
        List<Round> list = new ArrayList();
        Round dupRound = new Round();
        dupRound.setRoundNo(1);
        list.add(dupRound);
        when(season.getRounds()).thenReturn(list);
        when(season.getMaxOfRounds()).thenReturn(2);
        assertThrows(DuplicateKeyException.class, () -> service.execute());
        
    }
    
   
   
    
}
