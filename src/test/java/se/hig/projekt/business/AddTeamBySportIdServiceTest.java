/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.hig.projekt.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import se.hig.projekt.business.add.AddTeamBySportIdService;
import se.hig.projekt.dao.TeamDao;
import se.hig.projekt.domain.Sport;
import se.hig.projekt.domain.Team;

/**
 *
 * @author andreas
 */
public class AddTeamBySportIdServiceTest {

    @InjectMocks
    private AddTeamBySportIdService service;

    @Mock
    TeamDao dao;

    private Team team;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        team = new Team();

    }

    @After
    public void tearDown() {
    }

    @Test
    public void checkWithDuplicateName() {
        System.out.println("checkWithDuplicateName");
        List<Team> listOfTeams = new ArrayList();
        team = mock(Team.class);
        when(team.getName()).thenReturn("name");
        when(team.getSport()).thenReturn(new Sport());
        when(dao.findAll()).thenReturn(listOfTeams);

        listOfTeams.add(team);

        service.init(Map.of("team", team));
        assertThrows(IllegalArgumentException.class, () -> service.execute());
    }

    @Test
    public void failWithNameIsNull() {
        System.out.println("failWithNameIsNull");
        service.init(Map.of("team", team));
        assertThrows(NullPointerException.class, () -> service.execute());
    }

    @Test
    public void failWithInccorectNameLengthMin() {
        System.out.println("failWithInccorectNameLengthMin");
        team.setName("f");
        service.init(Map.of("team", team));
        assertThrows(IllegalArgumentException.class, () -> service.execute());
    }

    @Test
    public void failWithInccorrectNameLengthMax() {
        System.out.println("failWithInccorrectNameLengthMax");
        team.setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        service.init(Map.of("team", team));
        assertThrows(IllegalArgumentException.class, () -> service.execute());
    }

    @Test
    public void failWithSportIsNull() {
        System.out.println("failWithSportIsNull");
        team.setName("Fotboll");
        service.init(Map.of("team", team));
        assertThrows(NullPointerException.class, () -> service.execute());
    }
}
