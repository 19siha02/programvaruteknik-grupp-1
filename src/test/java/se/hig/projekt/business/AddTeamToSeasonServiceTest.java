/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.hig.projekt.business;

import se.hig.projekt.business.add.AddTeamToSeasonService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import se.hig.projekt.dao.SeasonDao;
import se.hig.projekt.dao.TeamDao;
import se.hig.projekt.domain.Season;
import se.hig.projekt.domain.Team;

/**
 *
 * @author Johannes Gyllenskepp || johannes.gyllenskepp@gmail.com
 */
public class AddTeamToSeasonServiceTest {
    
    @InjectMocks
    AddTeamToSeasonService service;
    
    @Mock
    TeamDao teamDao;
    
    @Mock
    SeasonDao seasonDao;
    
    Map map;
    private final Integer id = 1;
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        map = new HashMap<String, Integer>();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testExecuteToThrowNullPointerException() {
        map.put("season", id);
        map.put("team", id);
        when(seasonDao.findByIdOrThrow(id)).thenReturn(null);
        service.init(map);
        assertThrows(NullPointerException.class, () -> {
            service.execute();
        });
        
    }
    
    @Test
    public void testExecuteToThrow() {
        map.put("season", id);
        map.put("team", id);
        when(teamDao.findByIdOrThrow(id)).thenReturn(null);
        service.init(map);
        assertThrows(NullPointerException.class, () -> {
            service.execute();
        });
    }
    
    @Test
    public void testDuplicateName(){
        map.put("season", id);
        map.put("team", id);
        Team team = new Team();
        team.setName("GIF");
        Team team2 = new Team();
        team2.setName("GIF");
        team2.setSeasons(new ArrayList<Season>());
        Season season = new Season();
        List<Season> seasonList = new ArrayList();
        seasonList.add(season);
        team.setSeasons(seasonList);
        List<Team> teamList = new ArrayList<>();
        teamList.add(team);
        season.setTeams(teamList);
        when(teamDao.findByIdOrThrow(id)).thenReturn(team2);
        when(seasonDao.findByIdOrThrow(id)).thenReturn(season);
        service.init(map);
        assertThrows(IllegalArgumentException.class, () -> service.execute());
    }
    
}
