/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.hig.projekt.business;

import se.hig.projekt.business.add.AddNewSeasonService;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import se.hig.projekt.dao.SeasonDao;
import se.hig.projekt.domain.League;
import se.hig.projekt.domain.Season;

/**
 *
 * @author Simon Hall, Christian Bergqvist
 */
public class AddNewSeasonServiceTest {
    @InjectMocks
    private AddNewSeasonService service;
    
    @Mock
    private SeasonDao seasonDao;
    
    public AddNewSeasonServiceTest() {
    }
    
    Map<String, Season> map;
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        map = new HashMap();
        
        
    }
    
    @Test
    public void testNullNameThrows(){
        Season season = new Season();
        map.put("season", season);
        service.init(map);
        assertThrows(NullPointerException.class, () -> service.execute());
    }
    
    @Test
    public void testShortNameThrows(){
        League league = mock(League.class);
        Season season = mock(Season.class);
        when(season.getLeague()).thenReturn(league);
        when(season.getName()).thenReturn("g");
        map.put("season", season);
        service.init(map);
        assertThrows(IllegalArgumentException.class, () -> service.execute());
    }
    
    @Test
    public void testLongNameThrows(){
        League league = mock(League.class);
        Season season = mock(Season.class);
        when(season.getLeague()).thenReturn(league);
        when(season.getName()).thenReturn("aalikjhsglfasdkjfga,ksdjfgjkasdgfvasdnbvfasdjhgfasdjkhfgaskjhfgakjhgdsfjhvgkasdkjhgfgjhkasdfbvjhgkadsfgjhdsagjhkfdsfaghskjasdfkhg");
        map.put("season", season);
        service.init(map);
        assertThrows(IllegalArgumentException.class, () -> service.execute());
    }
    
    @Test
    public void testLeagueIsNull(){
        Season season = mock(Season.class);
        when(season.getLeague()).thenReturn(null);
        when(season.getName()).thenReturn("Fotboll");
        map.put("season", season);
        service.init(map);
        assertThrows(NullPointerException.class, () -> service.execute());
    }
    
    

    
    
}
