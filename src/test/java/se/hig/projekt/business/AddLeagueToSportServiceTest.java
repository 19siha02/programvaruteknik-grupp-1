/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.hig.projekt.business;

import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import se.hig.projekt.business.add.AddLeagueToSportService;
import se.hig.projekt.domain.League;
import se.hig.projekt.domain.Sport;

/**
 *
 * @author andreas
 */
public class AddLeagueToSportServiceTest {
    
    @InjectMocks
    private AddLeagueToSportService leagueService;
    
    private League league;
    
    public AddLeagueToSportServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
       MockitoAnnotations.initMocks(this);
       league = new League();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void failWithNullName() {
        System.out.println("failWithNullName");
        league.setSport(new Sport());
        leagueService.init(Map.of("league", league));
        Assertions.assertThrows(NullPointerException.class, () -> leagueService.execute());
    }
    
    @Test
    public void failwithNullSport() {
        System.out.println("failWithNullSport");
        league.setName("Fotboll");
        leagueService.init(Map.of("league", league));
        Assertions.assertThrows(NullPointerException.class, () -> leagueService.execute());
    }
    
    @Test
    public void failWithInccorectNameLengthMin() {
        System.out.println("failWithInccorectNameLengthMin");
        league.setName("f");
        league.setSport(new Sport());
        leagueService.init(Map.of("league", league));
        Assertions.assertThrows(IllegalArgumentException.class, () -> leagueService.execute());
    }
    
    @Test
    public void failWithInccorrectNameLengthMax() {
        System.out.println("failWithInccorrectNameLengthMax");
        league.setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        league.setSport(new Sport());
        leagueService.init(Map.of("league", league));
        Assertions.assertThrows(IllegalArgumentException.class, () -> leagueService.execute());
    }
    
    @Test
    public void failWithInccorrectPointsValue() {
        System.out.println("failWithInccorrectPointsValue");
        league.setName("Fotboll");
        league.setSport(new Sport());
        league.setWinValue(-1);
        leagueService.init(Map.of("league", league));
        Assertions.assertThrows(IllegalArgumentException.class, () -> leagueService.execute());
    }

    
   
    
}
